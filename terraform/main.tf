terraform {
  backend "s3" {}
}

data "terraform_remote_state" "state" {
  backend = "s3"

  config {
    bucket  = "${var.state_bucket}"
    region  = "${var.region}"
    key     = "${var.state_file}"
    profile = "agw"
  }
}

provider "aws" {
  region  = "${var.region}"
  profile = "agw"
}

provider "godaddy" {}

data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

data "aws_ami" "www-ami" {
  most_recent = true
  owners      = ["self"]

  filter {
    name   = "name"
    values = ["andrew wardrobe.com-www-*"]
  }
}

module "vpc" {
  source = "./modules/vpc"
  name   = "${var.vpc_name}"
}

resource "aws_route53_zone" "public" {
  name = "andrewwardrobe.com"
}

resource "godaddy_domain_record" "andrewwardrobe" {
  domain      = "andrewwardrobe.com"
  nameservers = ["${aws_route53_zone.public.name_servers}"]
}

resource "aws_route53_zone" "private" {
  name = "andrewwardrobe.com"

  vpc {
    vpc_id = "${module.vpc.id}"
  }
}

module "private_subnets" {
  source     = "./modules/multi_az_private_subnets"
  cidr_block = "${cidrsubnet(module.vpc.cidr_block ,3 ,1 )}"
  vpc_id     = "${module.vpc.id}"
  name       = "application_subnet"
}

module "public_subnets" {
  source     = "./modules/public_subnets"
  cidr_block = "${cidrsubnet(module.vpc.cidr_block ,3 ,0 )}"
  vpc_id     = "${module.vpc.id}"
  name       = "public_subnet"
}

resource "aws_eip" "bastion" {
  vpc      = true
  instance = "${module.bastion.instance_ids[0]}"
}

/*
module "nat_gateway" {
  source         = "./modules/nat_gateway"
  route_table_id = "${module.private_subnets.route_table_id}"
  subnet_id      = "${module.public_subnets.ids[0]}"
}
*/
module "bastion_security_group" {
  source = "./modules/bastion_security_group"
  vpc_id = "${module.vpc.id}"
}

module "private_security_group" {
  source                    = "./modules/private_security_group"
  vpc_id                    = "${module.vpc.id}"
  bastion_security_group_id = "${module.bastion_security_group.id}"
}

module "bastion" {
  source          = "./modules/ec2_with_dns"
  name            = "bastion"
  ami             = "${data.aws_ami.amazon-linux-2.id}"
  instance_type   = "t2.micro"
  subnet_ids      = ["${module.public_subnets.ids}"]
  route53_zone    = "${aws_route53_zone.public.zone_id}"
  num_instances   = "1"
  security_groups = ["${module.bastion_security_group.id}"]
  dns_entry       = false
  public_dns      = true
  key_name        = "${var.key}"
  roles           = ["bastion"]

  user_data = <<-EOF
    #!/bin/bash -ex
    perl -pi -e "s/^#?Port 22$/Port 22\nPort 443/" /etc/ssh/sshd_config
    service sshd restart || service ssh restart
    EOF
}

module "www_asg" {
  source          = "./modules/autoscaling_group"
  name            = "www"
  ami             = "${data.aws_ami.www-ami.id}"
  instance_type   = "t2.micro"
  user_data       = ""
  subnet_ids      = ["${module.private_subnets.ids}"]
  route53_zone    = "${aws_route53_zone.private.zone_id}"
  num_instances   = "1"
  security_groups = ["${module.private_security_group.id}"]
  key_name        = "${var.key}"
  public_dns      = false
  roles           = ["www"]
}

module "www_alb" {
  source                  = "./modules/application_load_balancer"
  name                    = "www"
  hosted_zone_id          = "${aws_route53_zone.public.zone_id}"
  vpc_id                  = "${module.vpc.id}"
  subnets                 = ["${module.public_subnets.ids}"]
  defult_target_group_arn = "${aws_alb_target_group.www_tg.arn}"
  certificate_arn         = "${module.www_ssl_cert.certificate_arn}"
}

resource "aws_alb_target_group" "www_tg" {
  name     = "www-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${module.vpc.id}"
}

module "www_ssl_cert" {
  source          = "./modules/ssl_cert"
  domain_name     = "www.andrewwardrobe.com"
  route53_zone_id = "${aws_route53_zone.public.id}"
}

resource "aws_autoscaling_attachment" "www" {
  autoscaling_group_name = "${module.www_asg.group_name}"
  alb_target_group_arn   = "${aws_alb_target_group.www_tg.arn}"
}

resource "aws_security_group_rule" "www_http" {
  from_port                = 80
  protocol                 = "tcp"
  source_security_group_id = "${module.www_alb.security_group_id}"
  security_group_id        = "${module.private_security_group.id}"
  to_port                  = 80
  type                     = "ingress"
}

resource "aws_security_group_rule" "www_https" {
  from_port                = 443
  protocol                 = "tcp"
  source_security_group_id = "${module.www_alb.security_group_id}"
  security_group_id        = "${module.private_security_group.id}"
  to_port                  = 443
  type                     = "ingress"
}

resource "aws_route53_record" "bastion" {
  name    = "bastion"
  type    = "A"
  ttl     = "300"
  zone_id = "${aws_route53_zone.public.zone_id}"
  records = ["${aws_eip.bastion.public_ip}"]
}
