output "bastion_ip" {
  value = "${aws_eip.bastion.public_ip}"
}

output "internet_gateway_id" {
  value = "${module.public_subnets.internet_gateway_id}"
}

output "vpc_id" {
  value = "${module.vpc.id}"
}

output "public_subnet_a_id" {
  value = "${element(module.public_subnets.ids, 0)}"
}

output "public_subnet_b_id" {
  value = "${element(module.public_subnets.ids, 1)}"
}

output "application_subnet_a_id" {
  value = "${element(module.private_subnets.ids, 0)}"
}

output "application_subnet_b_id" {
  value = "${element(module.private_subnets.ids, 1)}"
}

output "bastion_security_group_id" {
  value = "${module.bastion_security_group.id}"
}

output "application_security_group_id" {
  value = "${module.private_security_group.id}"
}

output "loadbalancer_security_group_id" {
  value = "${module.www_alb.security_group_id}"
}

output "default_security_group_id" {
  value = "${module.vpc.default_security_group_id}"
}

/*
output "nat_gateway_ip" {
  value = "${module.nat_gateway.ip_address}"
}

output "nat_gateway_id" {
  value = "${module.nat_gateway.gateway_id}"
}
*/

output "loadbalancer_arn" {
  value = "${module.www_alb.arn}"
}

output "loadbalancer_dns_name" {
  value = "${module.www_alb.dns_name}"
}

output "application_asg_arn" {
  value = "${module.www_asg.arn}"
}

output "amazon_ami_id" {
  value = "${data.aws_ami.amazon-linux-2.id}"
}

output "www_ami_id" {
  value = "${data.aws_ami.www-ami.id}"
}

output "www_launch_configuration" {
  value = "${module.www_asg.launch_configuration}"
}

output "www_asg" {
  value = "${module.www_asg.group_name}"
}