variable "region" {
  description = "AWS Region"
  default     = "eu-west-2"
}

variable "state_bucket" {
  description = "Bucket for storing state file"
}

variable "state_file" {
  description = "file for storing VPC state"
}

variable "key" {
  description = "SSH used for Provisioning Servers"
  default     = ""
}

variable "vpc_name" {
  default     = "main"
  description = "Name of VPC"
}
