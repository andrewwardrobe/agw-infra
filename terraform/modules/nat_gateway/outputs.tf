output "gateway_id" {
  value = "${aws_nat_gateway.main.id}"
}

output "ip_address" {
  value = "${aws_nat_gateway.main.public_ip}"
}
