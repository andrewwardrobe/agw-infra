resource "aws_security_group" "public" {
  vpc_id = "${var.vpc_id}"
  name   = "BastionSecurityGroup"

  tags {
    Name = "Public Security Group"
  }
}

resource "aws_security_group_rule" "ssh_out" {
  from_port         = 22
  protocol          = "tcp"
  security_group_id = "${aws_security_group.public.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  to_port           = 22
  type              = "egress"
}

resource "aws_security_group_rule" "ssh_in" {
  from_port         = 22
  protocol          = "tcp"
  security_group_id = "${aws_security_group.public.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  to_port           = 22
  type              = "ingress"
}

resource "aws_security_group_rule" "http_out" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = "${aws_security_group.public.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  to_port           = 80
  type              = "egress"
}

resource "aws_security_group_rule" "https_out" {
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.public.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  to_port           = 443
  type              = "egress"
}

resource "aws_security_group_rule" "https_in" {
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.public.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  to_port           = 443
  type              = "ingress"
}
