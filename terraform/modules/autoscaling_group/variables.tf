variable "subnet_ids" {
  description = "Subnets in which to place environment"
  type        = "list"
}

variable "security_groups" {
  description = "Securuty Groups"
  type        = "list"
}

variable "ami" {
  description = "AMI ID"
  default     = "ami-f976839e"
}

variable "name" {
  description = ""
}

variable "instance_type" {
  description = "Instance Type"
  default     = "m5.large"
}

variable "num_instances" {
  description = "number of instances"
}

variable "route53_zone" {
  description = "number of instances"
}

variable "key_name" {
  description = ""
  default     = ""
}

variable "user_data" {
  description = ""
  default     = ""
}

variable "root_disk_size" {
  description = "Size of the root disk"
  default     = "40"
}

variable "roles" {
  description = "instance role"
  default     = ["instance"]
  type        = "list"
}

variable "dns_entry" {
  description = "Put an entry in route 53"
  default     = true
}

variable "public_dns" {
  description = "Use a public DNS Entry"
  default     = false
}

variable "min_size" {
  description = "minimum size of autoscaling group"
  default     = 1
}

variable "max_size" {
  description = "maximum size of autoscaling group"
  default     = 1
}
