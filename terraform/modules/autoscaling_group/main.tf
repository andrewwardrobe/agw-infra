resource "aws_launch_configuration" "launch" {
  image_id      = "${var.ami}"
  instance_type = "${var.instance_type}"
  name_prefix   = "${var.name}_"

  key_name        = "${var.key_name}"
  security_groups = ["${var.security_groups}"]

  root_block_device = {
    volume_size = "${var.root_disk_size}"
    volume_type = "gp2"
  }

  lifecycle {
    create_before_destroy = true
  }

  #user_data = "${var.user_data}"
}

resource "aws_autoscaling_group" "asg" {
  name                 = "${aws_launch_configuration.launch.name}"
  max_size             = "${var.max_size}"
  min_size             = "${var.min_size}"
  desired_capacity     = "${var.num_instances}"
  vpc_zone_identifier  = ["${var.subnet_ids}"]
  launch_configuration = "${aws_launch_configuration.launch.name}"

  tags = [{
    key                 = "AutoScaleGroup"
    value               = "${var.name}"
    propagate_at_launch = true
  },
    {
      key                 = "Role"
      value               = "${join(",", var.roles)}"
      propagate_at_launch = true
    },
    {
      key                 = "Name"
      value               = "${var.name}"
      propagate_at_launch = true
    },
  ]

  lifecycle {
    create_before_destroy = true
  }
}
