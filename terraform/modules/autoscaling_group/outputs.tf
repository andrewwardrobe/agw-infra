#output "instance_ids" {
#  value = ["${aws_instance.instance.*.id}"]
#}

output "group_name" {
  value = "${aws_autoscaling_group.asg.name}"
}

output "arn" {
  value = "${aws_autoscaling_group.asg.arn}"
}

output "launch_configuration" {
  value = "${aws_launch_configuration.launch.name}"
}