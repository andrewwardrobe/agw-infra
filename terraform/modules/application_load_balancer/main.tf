resource "aws_security_group" "alb" {
  vpc_id = "${var.vpc_id}"
  name   = "${var.name} load balancer security group"
}

resource "aws_security_group_rule" "http_in" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = "${aws_security_group.alb.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  to_port           = 80
  type              = "ingress"
}

resource "aws_security_group_rule" "https_in" {
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.alb.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  to_port           = 443
  type              = "ingress"
}

resource "aws_security_group_rule" "http_out" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = "${aws_security_group.alb.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  to_port           = 80
  type              = "egress"
}

resource "aws_security_group_rule" "https_out" {
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.alb.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  to_port           = 443
  type              = "egress"
}

resource "aws_alb" "alb" {
  name               = "${var.name}-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.alb.id}"]
  subnets            = ["${var.subnets}"]
}

resource "aws_route53_record" "alb" {
  name    = "${var.name}"
  type    = "CNAME"
  ttl     = "300"
  zone_id = "${var.hosted_zone_id}"
  records = ["${aws_alb.alb.dns_name}"]
}

resource "aws_alb_listener" "http" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${var.defult_target_group_arn}"
  }
}

resource "aws_lb_listener_rule" "redirect_to_https" {
  listener_arn = "${aws_alb_listener.http.arn}"
  priority     = 100

  action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  condition {
    field  = "host-header"
    values = ["*.andrewwardrobe.com"]
  }
}

resource "aws_alb_listener" "https" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${var.certificate_arn}"

  default_action {
    type             = "forward"
    target_group_arn = "${var.defult_target_group_arn}"
  }
}
