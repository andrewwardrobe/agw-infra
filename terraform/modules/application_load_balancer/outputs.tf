output "arn" {
  value = "${aws_alb.alb.arn}"
}

output "security_group_id" {
  value = "${aws_security_group.alb.id}"
}

output "dns_name" {
  value = "${aws_alb.alb.dns_name}"
}
