resource "aws_instance" "instance" {
  count                  = "${var.num_instances}"
  ami                    = "${var.ami}"
  instance_type          = "${var.instance_type}"
  user_data              = "${var.user_data}"
  subnet_id              = "${element(var.subnet_ids, count.index % var.num_instances)}"
  vpc_security_group_ids = ["${var.security_groups}"]

  root_block_device = {
    volume_size = "${var.root_disk_size}"
    volume_type = "gp2"
  }

  tags {
    Name = "${var.name}${var.num_instances > 1 || var.name_as_prefix ? format("%02d", count.index +1 ) : ""}"
    Role = "${join(",", var.roles)}"
  }

  key_name = "${var.key_name}"
}

resource "aws_route53_record" "dns" {
  // same number of records as instances
  count   = "${var.dns_entry ? var.num_instances : 0 }"
  zone_id = "${var.route53_zone}"
  name    = "${var.name}${var.num_instances > 1 || var.name_as_prefix ? format("%02d", count.index +1 ) : ""}"
  type    = "A"
  ttl     = "300"

  // matches up record N to instance N
  records = ["${var.public_dns ? element(aws_instance.instance.*.public_ip, count.index) : element(aws_instance.instance.*.private_ip, count.index)}"]
}
