output "ids" {
  value = ["${aws_subnet.public_subnet_a.id}", "${aws_subnet.public_subnet_b.id}"]
}

output "route_table_id" {
  value = "${aws_route_table.public.id}"
}

output "internet_gateway_id" {
  value = "${aws_internet_gateway.main.id}"
}
