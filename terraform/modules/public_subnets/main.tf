resource "aws_subnet" "public_subnet_a" {
  cidr_block        = "${cidrsubnet(var.cidr_block ,1 ,0 )}"
  availability_zone = "${var.region}a"
  vpc_id            = "${var.vpc_id}"

  tags {
    Name = "${var.name}_a"
  }
}

resource "aws_subnet" "public_subnet_b" {
  cidr_block        = "${cidrsubnet(var.cidr_block ,1 ,1 )}"
  availability_zone = "${var.region}b"
  vpc_id            = "${var.vpc_id}"

  tags {
    Name = "${var.name}_b"
  }
}

resource "aws_route_table" "public" {
  vpc_id = "${var.vpc_id}"

  tags {
    Name = "Public Route Table"
  }
}

# Internet access
resource "aws_internet_gateway" "main" {
  vpc_id = "${var.vpc_id}"

  tags {
    Network = "Public"
    Name    = "Internet Gateway"
  }
}

resource "aws_route" "public_ingress" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

resource "aws_route_table_association" "public_a" {
  route_table_id = "${aws_route_table.public.id}"
  subnet_id      = "${aws_subnet.public_subnet_a.id}"
}

resource "aws_route_table_association" "public_b" {
  route_table_id = "${aws_route_table.public.id}"
  subnet_id      = "${aws_subnet.public_subnet_b.id}"
}

resource "aws_network_acl" "public" {
  vpc_id     = "${var.vpc_id}"
  subnet_ids = ["${aws_subnet.public_subnet_a.id}", "${aws_subnet.public_subnet_b.id}"]

  egress {
    rule_no    = "100"
    from_port  = "0"
    to_port    = "0"
    action     = "ALLOW"
    cidr_block = "0.0.0.0/0"
    protocol   = "-1"
  }

  ingress {
    rule_no    = "100"
    from_port  = "0"
    to_port    = "0"
    action     = "ALLOW"
    cidr_block = "0.0.0.0/0"
    protocol   = "-1"
  }

  tags {
    Name = "Public NACL"
  }
}
