variable "vpc_id" {
  description = "vpc_id"
}

variable "name" {
  description = "subnet name"
  default     = "Private Subnet"
}

variable "cidr_block" {
  description = "CIDR block"
}

variable "region" {
  description = "reion in which to place subnets"
  default     = "eu-west-2"
}
