resource "aws_subnet" "private_subnet_a" {
  cidr_block        = "${cidrsubnet(var.cidr_block,1,0)}"
  vpc_id            = "${var.vpc_id}"
  availability_zone = "${var.region}a"

  tags {
    Name = "${var.name}_a"
  }
}

resource "aws_subnet" "private_subnet_b" {
  cidr_block        = "${cidrsubnet(var.cidr_block,1,1)}"
  vpc_id            = "${var.vpc_id}"
  availability_zone = "${var.region}b"

  tags {
    Name = "${var.name}_b"
  }
}

resource "aws_route_table" "private" {
  vpc_id = "${var.vpc_id}"

  tags {
    Name = "Private Route Table"
  }
}

resource "aws_route_table_association" "private_a" {
  route_table_id = "${aws_route_table.private.id}"
  subnet_id      = "${aws_subnet.private_subnet_a.id}"
}

resource "aws_route_table_association" "private_b" {
  route_table_id = "${aws_route_table.private.id}"
  subnet_id      = "${aws_subnet.private_subnet_b.id}"
}

resource "aws_network_acl" "private" {
  vpc_id     = "${var.vpc_id}"
  subnet_ids = ["${aws_subnet.private_subnet_a.id}", "${aws_subnet.private_subnet_b.id}"]

  egress {
    rule_no    = "100"
    from_port  = "0"
    to_port    = "0"
    action     = "ALLOW"
    cidr_block = "0.0.0.0/0"
    protocol   = "-1"
  }

  ingress {
    rule_no    = "100"
    from_port  = "0"
    to_port    = "0"
    action     = "ALLOW"
    cidr_block = "0.0.0.0/0"
    protocol   = "-1"
  }

  tags {
    Name = "Private NACL"
  }
}
