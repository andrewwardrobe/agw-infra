output "ids" {
  value = ["${aws_subnet.private_subnet_a.id}", "${aws_subnet.private_subnet_b.id}"]
}

output "route_table_id" {
  value = "${aws_route_table.private.id}"
}
