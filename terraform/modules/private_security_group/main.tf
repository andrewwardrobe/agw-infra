resource "aws_security_group" "private" {
  vpc_id = "${var.vpc_id}"
  name   = "private security group"

  tags {
    Name = "Private Security Group"
  }
}

resource "aws_security_group_rule" "ssh_in" {
  from_port                = 22
  protocol                 = "tcp"
  source_security_group_id = "${var.bastion_security_group_id}"
  security_group_id        = "${aws_security_group.private.id}"
  to_port                  = 22
  type                     = "ingress"
}

resource "aws_security_group_rule" "http_out" {
  from_port         = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.private.id}"
  to_port           = 80
  type              = "egress"
}

resource "aws_security_group_rule" "https_out" {
  from_port         = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.private.id}"
  to_port           = 443
  type              = "egress"
}
