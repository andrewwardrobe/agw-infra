require 'rspec/core/rake_task'
require_relative '../rake/helpers'
require_relative '../spec/helpers/terraform_helpers'

task :generate do
  vpc_id = Terraform::Output.vpc
  types = %w[vpc nat_gateway internet_gateway network_acl network_interface subnet security_group route_table
          alb autoscaling_group ec2 eip]
  types.each do |type|
    run_command "awspec generate #{type} #{vpc_id}"
  end

end