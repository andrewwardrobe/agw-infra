require 'rspec/core/rake_task'
require_relative '../rake/helpers'

desc 'build base ami'
task :base do
  Dir.chdir('packer') do
    run_command 'packer build base.json'
  end
end