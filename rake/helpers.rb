require 'open3'

def run_command(command)
  Open3.popen2e command do |_stdin, stdout_and_stderr, _wait_thr|
    while (line = stdout_and_stderr.gets) do
      puts line
    end
    raise "Command `#{command}' failed'"  unless $?.success?
  end
end