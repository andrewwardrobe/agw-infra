require 'yaml'
require 'json'
require 'rspec/core/rake_task'
require_relative '../rake/helpers'

not_playbooks = ['inventory']

def ansible_playbook(playbook)
  desc "Run the #{playbook} playbook"
  task playbook.to_sym do
    Dir.chdir 'ansible' do
      run_command "ansible-playbook #{playbook}.yml"
    end
  end
end

desc 'Run ansible-inventory'
task :inventory do
  Dir.chdir 'ansible' do
    data = JSON.parse(%x{ansible-inventory --list})
    puts data.to_yaml
  end
end

Dir['ansible/*.yml'].each do |pb|
  playbook = pb.split('.')[0].split('/')[1]
  next if not_playbooks.include? (playbook)
  ansible_playbook playbook
end