require 'rspec/core/rake_task'
require_relative '../rake/helpers'

desc "Run terraform init"
task :init do
  Dir.chdir 'terraform' do
    run_command('terraform init -backend-config=backend.config')
  end
end

desc "create terraform plan"
task :plan do
  Dir.chdir 'terraform' do
    run_command('terraform plan -out tfplan.out')
  end
end

desc "create terraform destroy plan"
task :dplan do
  Dir.chdir 'terraform' do
    run_command('terraform plan -out tfplan.out -destroy')
  end

end

desc "Run terraform destroy"
task :destroy do
  Dir.chdir 'terraform' do
    run_command('terraform destroy -force')
  end
end

desc "Run terraform apply"
task :apply do
  Dir.chdir 'terraform' do
    run_command('terraform apply tfplan.out')
  end
end

desc "Run terraform output"
task :output do
  Dir.chdir 'terraform' do
    run_command('terraform output')
  end
end