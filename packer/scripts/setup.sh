#!/usr/bin/env bash

sudo yum update -y

sudo amazon-linux-extras enable epel
sudo yum install -y epel-release
sudo yum localinstall -y https://releases.ansible.com/ansible/rpm/release/epel-7-x86_64/ansible-2.7.9-1.el7.ans.noarch.rpm

