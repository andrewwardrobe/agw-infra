#####To Run the terraform 

* Update terraform.tfvars and backned.config with your bucket name
* Run `terraform init -backend-config=backend.config terraform`
* Then `terraform plan terraform`
* Then `terraform apply "terraform.plan"`

#####To run the Tests
```
bundle install
bundle exec rake spec
```  
##### Dependencies
Bundler gem and ruby 2.0+ should be install on the system running the test (rather than the target system)
