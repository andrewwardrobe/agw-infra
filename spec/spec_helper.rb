require 'serverspec_launcher/spec_helper'
require 'serverspec_extra_types'
require 'awspec'


Awsecrets.load(secrets_path: File.expand_path('./secrets.yml', File.dirname(__FILE__)))
