require 'spec_helper'
require 'helpers/terraform_helpers'
require 'helpers/aws_helpers'

describe vpc('main') do
  it { should exist }
  it { should be_available }
  its(:vpc_id) { should eq Terraform::Output.vpc }
  its(:cidr_block) { should eq '192.168.0.0/21' }
  it { should have_route_table('Public Route Table') }
  it { should have_route_table('Private Route Table') }
  it { should have_route_table('Default Route Table for VPC main') }
  it { should have_network_acl('Public NACL') }
  it { should have_network_acl('Private NACL') }
  it { should have_network_acl('Default NACL for VPC main') }
  it { should have_vpc_attribute('enableDnsHostnames') }
  it { should have_vpc_attribute('enableDnsSupport') }
end

=begin
describe nat_gateway(Terraform::Output.nat_gateway) do
  it { should exist }
  it { should be_available }
  it { should belong_to_vpc('main') }
  it { should have_eip(Terraform::Output.nat_gateway_ip) }
end
=end

describe internet_gateway(Terraform::Output.internet_gateway) do
  it { should exist }
  it { should be_attached_to('main') }
end


describe network_acl('Public NACL') do
  it { should exist }
  it { should belong_to_vpc('main') }
  it { should have_subnet('public_subnet_a') }
  it { should have_subnet('public_subnet_a') }
  its(:outbound) { should be_allowed.protocol('ALL').source('0.0.0.0/0').rule_number(100) }
  its(:outbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound) { should be_allowed.protocol('ALL').source('0.0.0.0/0').rule_number(100) }
  its(:inbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound_entries_count) { should eq 2 }
  its(:outbound_entries_count) { should eq 2 }
end

describe network_acl('Private NACL') do
  it { should exist }
  it { should belong_to_vpc('main') }
  it { should have_subnet('application_subnet_a') }
  it { should have_subnet('application_subnet_b') }
  its(:outbound) { should be_allowed.protocol('ALL').source('0.0.0.0/0').rule_number(100) }
  its(:outbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound) { should be_allowed.protocol('ALL').source('0.0.0.0/0').rule_number(100) }
  its(:inbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound_entries_count) { should eq 2 }
  its(:outbound_entries_count) { should eq 2 }
end

describe network_acl('Default NACL for VPC main') do
  it { should exist }
  it { should belong_to_vpc('main') }
  its(:outbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound_entries_count) { should eq 1 }
  its(:outbound_entries_count) { should eq 1 }
end



describe subnet('public_subnet_a') do
  it { should exist }
  it { should be_available }
  it { should belong_to_vpc('main') }
  its(:subnet_id) { should eq Terraform::Output.public_subnet_a }
  its(:cidr_block) { should eq '192.168.0.0/25' }
end

describe subnet('public_subnet_b') do
  it { should exist }
  it { should be_available }
  it { should belong_to_vpc('main') }
  its(:subnet_id) { should eq Terraform::Output.public_subnet_b }
  its(:cidr_block) { should eq '192.168.0.128/25' }
end


describe subnet('application_subnet_a') do
  it { should exist }
  it { should be_available }
  it { should belong_to_vpc('main') }
  its(:subnet_id) { should eq Terraform::Output.application_subnet_a }
  its(:cidr_block) { should eq '192.168.1.0/25' }
end

describe subnet('application_subnet_b') do
  it { should exist }
  it { should be_available }
  it { should belong_to_vpc('main') }
  its(:subnet_id) { should eq Terraform::Output.application_subnet_b }
  its(:cidr_block) { should eq '192.168.1.128/25' }
end


describe security_group('private security group') do
  it { should exist }
  its(:group_id) { should eq Terraform::Output.application_security_group }
  its(:group_name) { should eq 'private security group' }
  its(:inbound) { should be_opened(80).protocol('tcp').for('www load balancer security group') }
  its(:inbound) { should be_opened(22).protocol('tcp').for('BastionSecurityGroup') }
  its(:inbound) { should be_opened(443).protocol('tcp').for('www load balancer security group') }
  its(:outbound) { should be_opened(80).protocol('tcp').for('0.0.0.0/0') }
  its(:outbound) { should be_opened(443).protocol('tcp').for('0.0.0.0/0') }
  its(:inbound_rule_count) { should eq 3 }
  its(:outbound_rule_count) { should eq 2 }
  its(:inbound_permissions_count) { should eq 3 }
  its(:outbound_permissions_count) { should eq 2 }
  it { should belong_to_vpc('main') }
end

describe security_group('BastionSecurityGroup') do
  it { should exist }
  its(:group_id) { should eq Terraform::Output.bastion_security_group }
  its(:group_name) { should eq 'BastionSecurityGroup' }
  its(:inbound) { should be_opened(22).protocol('tcp').for('0.0.0.0/0') }
  its(:outbound) { should be_opened(80).protocol('tcp').for('0.0.0.0/0') }
  its(:outbound) { should be_opened(22).protocol('tcp').for('0.0.0.0/0') }
  its(:outbound) { should be_opened(443).protocol('tcp').for('0.0.0.0/0') }
  its(:inbound_rule_count) { should eq 2 }
  its(:outbound_rule_count) { should eq 3 }
  its(:inbound_permissions_count) { should eq 2 }
  its(:outbound_permissions_count) { should eq 3 }
  it { should belong_to_vpc('main') }
end

describe security_group('main_default_security_group') do
  it { should exist }
  its(:group_id) { should eq Terraform::Output.default_security_group }
  its(:group_name) { should eq 'default' }
  # its(:inbound) { should be_opened.protocol('all').for('main_default_security_group') }
  # its(:outbound) { should be_opened.protocol('all').for('0.0.0.0/0') }
  # its(:inbound_rule_count) { should eq 1 }
  # its(:outbound_rule_count) { should eq 1 }
  # its(:inbound_permissions_count) { should eq 1 }
  # its(:outbound_permissions_count) { should eq 1 }
  it { should belong_to_vpc('main') }
end

describe security_group('www load balancer security group') do
  it { should exist }
  its(:group_id) { should eq Terraform::Output.loadbalancer_security_group }
  its(:group_name) { should eq 'www load balancer security group' }
  its(:inbound) { should be_opened(80).protocol('tcp').for('0.0.0.0/0') }
  its(:inbound) { should be_opened(443).protocol('tcp').for('0.0.0.0/0') }
  its(:outbound) { should be_opened(80).protocol('tcp').for('0.0.0.0/0') }
  its(:outbound) { should be_opened(443).protocol('tcp').for('0.0.0.0/0') }
  its(:inbound_rule_count) { should eq 2 }
  its(:outbound_rule_count) { should eq 2 }
  its(:inbound_permissions_count) { should eq 2 }
  its(:outbound_permissions_count) { should eq 2 }
  it { should belong_to_vpc('main') }
  end


describe route_table('Public Route Table') do
  it { should exist }
  it { should belong_to_vpc('main') }
  it { should have_route('192.168.0.0/21').target(gateway: 'local') }
  it { should have_route('0.0.0.0/0').target(gateway: Terraform::Output.internet_gateway) }
  it { should have_subnet('public_subnet_a') }
  it { should have_subnet('public_subnet_b') }
end


describe route_table('Private Route Table') do
  it { should exist }
  it { should belong_to_vpc('main') }
  it { should have_route('192.168.0.0/21').target(gateway: 'local') }
#  it { should have_route('0.0.0.0/0').target(nat: Terraform::Output.nat_gateway) }
  it { should have_subnet('application_subnet_a') }
end


describe route_table('Default Route Table for VPC main') do
  it { should exist }
  it { should belong_to_vpc('main') }
  it { should have_route('192.168.0.0/21').target(gateway: 'local') }
end


describe alb('www-lb') do
  it { should exist }
  its(:load_balancer_arn) { should eq Terraform::Output.loadbalancer_arn }
  its(:dns_name) { should eq Terraform::Output.loadbalancer_dns_name }
  its(:load_balancer_name) { should eq 'www-lb' }
  its(:scheme) { should eq 'internet-facing' }
  its(:vpc_id) { should eq Terraform::Output.vpc }
  its(:type) { should eq 'application' }
  its(:ip_address_type) { should eq 'ipv4' }
end


describe autoscaling_group(Terraform::Output.www_asg) do
  it { should exist }
  its(:auto_scaling_group_name) { should match /www_/ }
  its(:auto_scaling_group_arn) { should eq Terraform::Output.application_asg_arn }
  its(:min_size) { should eq 1 }
  its(:max_size) { should eq 1 }
  its(:desired_capacity) { should eq 1 }
  its(:default_cooldown) { should eq 300 }
  its(:availability_zones) { should eq ["eu-west-2a", "eu-west-2b"] }
  its(:health_check_type) { should eq 'EC2' }
  its(:health_check_grace_period) { should eq 300 }
  its(:vpc_zone_identifier) { should eq("#{Terraform::Output.application_subnet_a},#{Terraform::Output.application_subnet_b}").or eq("#{Terraform::Output.application_subnet_b},#{Terraform::Output.application_subnet_a}") }
  its(:termination_policies) { should eq ["Default"] }
  its(:new_instances_protected_from_scale_in) { should eq false }
  it { should have_launch_configuration(Terraform::Output.www_launch_configuration) }
  it { should have_alb_target_group('www-tg') }
end



describe ec2('bastion') do
  it { should exist }
  it { should be_running }
  its(:image_id) { should eq Terraform::Output.amazon_ami }
  its(:instance_type) { should eq 't2.micro' }
  its(:public_ip_address) { should eq Terraform::Output.bastion_ip }
  it { should have_security_group('BastionSecurityGroup') }
  it { should belong_to_vpc('main') }
  it { should belong_to_subnet('public_subnet_a').or belong_to_subnet('public_subnet_b')}
  it { should have_eip(Terraform::Output.bastion_ip) }
end


autoscale_instances(Terraform::Output.www_asg).each do |instance_id|
  describe ec2(instance_id) do
    it { should exist }
    it { should be_running }
    its(:instance_id) { should eq instance_id}
    its(:image_id) { should eq Terraform::Output.www_ami }
    its(:instance_type) { should eq 't2.micro' }
    it { should have_security_group(Terraform::Output.application_security_group) }
    it { should belong_to_vpc('main') }
    it { should belong_to_subnet('application_subnet_a').or belong_to_subnet('application_subnet_b') }
  end
end