require 'spec_helper'
require 'helpers/terraform_helpers'


describe curl("http://www.andrewwardrobe.com") do
  it { should respond_with_MOVED_PERMANENTLY }
end

describe curl("https://www.andrewwardrobe.com") do
  it { should respond_with_OK }
end

describe curl("http://www.andrewwardrobe.com", follow_redirects: true) do
  its(:body) { should match /Hello World/ }
end

describe curl(Terraform::Output.loadbalancer_dns_name) do
  its(:body) { should match /Hello World/ }
end