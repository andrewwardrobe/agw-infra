require 'spec_helper'

describe package('docker') do
  it { should be_installed }
end

describe docker_image('andrewwardrobe/hello-world:latest') do
  it { should exist }
end

describe docker_container('hello_world') do
  it { should exist }
  it { should be_running }
  it { should have_image('andrewwardrobe/hello-world:latest') }
  it { should map_port('80', '80')}
end