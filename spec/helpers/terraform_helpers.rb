module Terraform
  module Output
    require 'open3'
    def self.output(output_id)
      cwd = Dir.pwd
      Dir.chdir 'terraform/'
      out, err, result = Open3.capture3("terraform", "output", output_id)
      Dir.chdir cwd
      out.chomp
    end

    def self.amazon_ami
      self.output 'amazon_ami_id'
    end

    def self.www_ami
      self.output 'www_ami_id'
    end


    def self.vpc
      self.output 'vpc_id'
    end

    def self.nat_gateway
      self.output 'nat_gateway_id'
    end

    def self.nat_gateway_ip
      self.output 'nat_gateway_ip'
    end

    def self.bastion_ip
      self.output 'bastion_ip'
    end

    def self.internet_gateway
      self.output 'internet_gateway_id'
    end

    def self.public_subnet_a
      self.output 'public_subnet_a_id'
    end

    def self.public_subnet_b
      self.output 'public_subnet_b_id'
    end

    def self.application_subnet_a
      self.output 'application_subnet_a_id'
    end

    def self.application_subnet_b
      self.output 'application_subnet_b_id'
    end

    def self.bastion_security_group
      self.output 'bastion_security_group_id'
    end

    def self.default_security_group
      self.output 'default_security_group_id'
    end

    def self.application_security_group
      self.output 'application_security_group_id'
    end

    def self.loadbalancer_security_group
      self.output 'loadbalancer_security_group_id'
    end

    def self.loadbalancer_arn
      self.output 'loadbalancer_arn'
    end

    def self.application_asg_arn
      self.output 'application_asg_arn'
    end

    def self.loadbalancer_dns_name
      self.output 'loadbalancer_dns_name'
    end

    def self.www_asg
      self.output 'www_asg'
    end

    def self.www_launch_configuration
      self.output 'www_launch_configuration'
    end
  end
end
