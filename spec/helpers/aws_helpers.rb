require 'aws-sdk-autoscaling'


def autoscale_instances(autoscale_group)
  as_client = Aws::AutoScaling::Client.new()
  group = as_client.describe_auto_scaling_groups(auto_scaling_group_names: [autoscale_group])[0][0]
  group[:instances].map {|i| i[:instance_id]}
end
